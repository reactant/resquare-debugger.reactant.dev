import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadsTimelineComponent } from './threads-timeline.component';

describe('ThreadsTimelineComponent', () => {
  let component: ThreadsTimelineComponent;
  let fixture: ComponentFixture<ThreadsTimelineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThreadsTimelineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadsTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
