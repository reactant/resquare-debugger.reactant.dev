import {
  AfterViewChecked, ChangeDetectorRef, Component, ElementRef, EventEmitter, OnChanges, OnInit, Output, ViewChild,
} from '@angular/core';
import { DebuggerProfilerService } from 'src/app/services/debugger/debugger-profiler.service';
import { NgScrollbar } from 'ngx-scrollbar';
import { DebuggerProfilerRenderTaskModel } from 'src/app/models/debugger-profiler-report-model';

@Component({
  selector: 'app-threads-timeline',
  templateUrl: './threads-timeline.component.html',
  styleUrls: ['./threads-timeline.component.scss'],
})
export class ThreadsTimelineComponent implements OnInit, OnChanges, AfterViewChecked {
  constructor(
    readonly debuggerProfilerService: DebuggerProfilerService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {

  }

  @Output() selectTask = new EventEmitter<DebuggerProfilerRenderTaskModel>();

  @ViewChild('scaleContainer') scaleContainer!: NgScrollbar;
  @ViewChild('scaleContent', { read: ElementRef }) scaleContent!: ElementRef<HTMLDivElement>;

  contentScale = 1;
  private newScrollLeft: number | null = null;

  onWheel($event: Event): void {
    const mouseEvent = $event as WheelEvent;
    if (mouseEvent.deltaY === 0) {
      return;
    }
    console.log($event);
    const pointerOffsetXPercentage = (mouseEvent.clientX - 8) / this.scaleContainer.nativeElement.clientWidth;

    const areaScale = (mouseEvent.deltaY < 0) ? 0.85 : (1 / 0.85);


    const oldVisibleContentAreaScale = 1 / this.contentScale;
    // const oldScrollLeftScale = this.scaleContainer.nativeElement.scrollLeft / this.scaleContent.nativeElement.clientWidth;
    const oldScrollLeftScale = this.scaleContainer.viewport.scrollLeft / this.scaleContent.nativeElement.clientWidth;

    const newVisibleContentAreaScale = Math.min(1, oldVisibleContentAreaScale * areaScale);
    const visibleContentAreaScaleChange = newVisibleContentAreaScale - oldVisibleContentAreaScale;
    const newScrollLeftScale = oldScrollLeftScale + (-visibleContentAreaScaleChange * pointerOffsetXPercentage);


    this.contentScale = 1 / newVisibleContentAreaScale;
    const newContentWidth = this.scaleContainer.nativeElement.clientWidth * this.contentScale;
    this.newScrollLeft = newScrollLeftScale * newContentWidth;
    console.log(`${newScrollLeftScale} * ${this.scaleContainer.nativeElement.clientWidth} * ${this.contentScale} = ${this.newScrollLeft}`);


  }

  ngOnChanges(): void {
  }

  ngAfterViewChecked(): void {
    if (this.newScrollLeft != null) {
      this.scaleContainer.scrollTo({ left: this.newScrollLeft, duration: 0 });
      this.newScrollLeft = null;
      this.changeDetectorRef.detectChanges();
      console.log('scroll');
    }
  }

  getPercentageByNanotime(nanotime: number): number {
    // tslint:disable-next-line:no-non-null-assertion
    return 100 * (nanotime - this.debuggerProfilerService.profilerResult!.totalTimePeriod.startTime!) /
      this.debuggerProfilerService.profilerResultTotalNanosecond;
  }

  ngOnInit(): void {
  }

}
