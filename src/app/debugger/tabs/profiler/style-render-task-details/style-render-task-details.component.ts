import { Component, Input, OnInit } from '@angular/core';
import { DebuggerProfilerStyleRenderTaskModel } from 'src/app/models/debugger-profiler-report-model';

@Component({
  selector: 'app-style-render-task-details',
  templateUrl: './style-render-task-details.component.html',
  styleUrls: ['./style-render-task-details.component.scss'],
})
export class StyleRenderTaskDetailsComponent implements OnInit {

  constructor() {
  }

  @Input()
  task!: DebuggerProfilerStyleRenderTaskModel;

  ngOnInit(): void {
  }

  getPercentageByNanotime(nanotime: number): number {
    // tslint:disable-next-line:no-non-null-assertion
    return 100 * (nanotime - this.task.totalTimePeriod.startTime!) /
      // tslint:disable-next-line:no-non-null-assertion
      (this.task.totalTimePeriod.endTime! - this.task.totalTimePeriod.startTime!);
  }
}
