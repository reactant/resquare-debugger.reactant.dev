import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleRenderTaskDetailsComponent } from './style-render-task-details.component';

describe('StyleRenderTaskDetailsComponent', () => {
  let component: StyleRenderTaskDetailsComponent;
  let fixture: ComponentFixture<StyleRenderTaskDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StyleRenderTaskDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleRenderTaskDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
