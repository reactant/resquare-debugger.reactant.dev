import { Component, Input, OnInit } from '@angular/core';
import { DebuggerProfilerDOMRenderTaskIterationModel } from 'src/app/models/debugger-profiler-report-model';

@Component({
  selector: 'app-dom-render-task-iteration',
  templateUrl: './dom-render-task-iteration.component.html',
  styleUrls: ['./dom-render-task-iteration.component.scss'],
})
export class DomRenderTaskIterationComponent implements OnInit {

  constructor() {
  }

  @Input()
  renderIteration!: DebuggerProfilerDOMRenderTaskIterationModel;

  ngOnInit(): void {
  }

}
