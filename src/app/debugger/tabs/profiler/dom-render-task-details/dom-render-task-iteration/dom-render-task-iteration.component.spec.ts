import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DomRenderTaskIterationComponent } from 'src/app/debugger/tabs/profiler/dom-render-task-details/dom-render-task-iteration/dom-render-task-iteration.component';

describe('DomRenderTaskIterationComponent', () => {
  let component: DomRenderTaskIterationComponent;
  let fixture: ComponentFixture<DomRenderTaskIterationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DomRenderTaskIterationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DomRenderTaskIterationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
