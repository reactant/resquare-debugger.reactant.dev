import { Component, Input, OnInit } from '@angular/core';
import { DebuggerProfilerDOMRenderTaskModel } from 'src/app/models/debugger-profiler-report-model';

@Component({
  selector: 'app-dom-render-task-details',
  templateUrl: './dom-render-task-details.component.html',
  styleUrls: ['./dom-render-task-details.component.scss'],
})
export class DomRenderTaskDetailsComponent implements OnInit {

  constructor() {
  }

  @Input()
  task!: DebuggerProfilerDOMRenderTaskModel;

  ngOnInit(): void {
  }

}
