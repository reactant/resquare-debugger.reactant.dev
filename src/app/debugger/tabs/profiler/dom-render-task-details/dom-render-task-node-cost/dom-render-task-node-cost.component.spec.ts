import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DomRenderTaskNodeCostComponent } from './dom-render-task-node-cost.component';

describe('DomRenderTaskNodeCostComponent', () => {
  let component: DomRenderTaskNodeCostComponent;
  let fixture: ComponentFixture<DomRenderTaskNodeCostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DomRenderTaskNodeCostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DomRenderTaskNodeCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
