import { Component, Input, OnInit } from '@angular/core';
import {
  DebuggerProfilerDOMRenderTaskIterationModel, DebuggerProfilerNodeRenderStateModel, DebuggerProfilerNodeStateRenderInfoModel,
  DebuggerTaskTimePeriodModel,
} from 'src/app/models/debugger-profiler-report-model';
import { DebuggerProfilerService } from 'src/app/services/debugger/debugger-profiler.service';

@Component({
  selector: 'app-dom-render-task-node-cost',
  templateUrl: './dom-render-task-node-cost.component.html',
  styleUrls: ['./dom-render-task-node-cost.component.scss'],
})
export class DomRenderTaskNodeCostComponent implements OnInit {

  constructor(readonly debuggerProfilerService: DebuggerProfilerService) {
  }

  @Input()
  nodeRenderState!: DebuggerProfilerNodeRenderStateModel;

  @Input()
  domRenderTaskIteration!: DebuggerProfilerDOMRenderTaskIterationModel;

  @Input()
  containerTimePeriod!: DebuggerTaskTimePeriodModel;

  getPercentageByNanotime(nanotime: number): number {
    // tslint:disable-next-line:no-non-null-assertion
    return 100 * (nanotime - this.containerTimePeriod.startTime!) /
      // tslint:disable-next-line:no-non-null-assertion
      (this.containerTimePeriod.endTime! - this.containerTimePeriod.startTime!);
  }

  get description(): string {
    if (this.nodeRenderInfo == null) {
      return `${this.nodeRenderState.name} (render skipped)`;
    } else {
      // tslint:disable-next-line:no-non-null-assertion
      return `${this.nodeRenderState.name} (${(this.nodeTimePeriod.endTime! - this.nodeTimePeriod.startTime!) / 1000}µs) [${this.nodeRenderInfo.renderReason}]`;
    }
  }

  get nodeTimePeriod(): DebuggerTaskTimePeriodModel {
    return this.domRenderTaskIteration.nodeStateIdTimePeriodMap[this.nodeRenderState.id];
  }

  get nodeRenderInfo(): DebuggerProfilerNodeStateRenderInfoModel {
    return this.domRenderTaskIteration.nodeStateIdInfoMap[this.nodeRenderState.id];
  }

  ngOnInit(): void {
  }

}
