import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DomRenderTaskDetailsComponent } from './dom-render-task-details.component';

describe('DomRenderTaskDetailsComponent', () => {
  let component: DomRenderTaskDetailsComponent;
  let fixture: ComponentFixture<DomRenderTaskDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DomRenderTaskDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DomRenderTaskDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
