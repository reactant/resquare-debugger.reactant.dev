import { Component, OnInit } from '@angular/core';
import { DebuggerProfilerService } from 'src/app/services/debugger/debugger-profiler.service';
import { ProgressBarMode } from '@angular/material/progress-bar/progress-bar';
import {
  DebuggerProfilerDOMRenderTaskModel, DebuggerProfilerRenderTaskModel, DebuggerProfilerStyleRenderTaskModel,
} from 'src/app/models/debugger-profiler-report-model';

@Component({
  selector: 'app-profiler',
  templateUrl: './profiler.component.html',
  styleUrls: ['./profiler.component.scss'],
})
export class ProfilerComponent implements OnInit {

  constructor(
    readonly debuggerProfilerService: DebuggerProfilerService,
  ) {
  }

  selectedRenderTask: DebuggerProfilerRenderTaskModel | null = null;

  get selectedDOMRenderTask(): DebuggerProfilerDOMRenderTaskModel | null {
    return this.selectedRenderTask?.type === 'DOMRenderTask' ? this.selectedRenderTask as DebuggerProfilerDOMRenderTaskModel : null;
  }

  get selectedStyleRenderTask(): DebuggerProfilerStyleRenderTaskModel | null {
    return this.selectedRenderTask?.type === 'StyleRenderTask' ? this.selectedRenderTask as DebuggerProfilerStyleRenderTaskModel : null;
  }

  ngOnInit(): void {
  }

  get progressBarMode(): ProgressBarMode | null {
    if (this.debuggerProfilerService.isProfiling) {
      return 'buffer';
    } else if (this.debuggerProfilerService.isWaitingForProfilingResult) {
      return 'indeterminate';
    }
    return null;
  }

  onProfilerControlButtonClick(): void {
    if (this.debuggerProfilerService.isWaitingForProfilingResult) {
      return;
    } else if (this.debuggerProfilerService.isProfiling) {
      this.debuggerProfilerService.stopProfiling();
    } else {
      this.debuggerProfilerService.startProfiling();
    }
  }


}
