import { Component, DoCheck, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DebuggerStyleRenderResultModel } from 'src/app/models/debugger-root-container.model';
import { DebuggerElementsService } from 'src/app/services/debugger/debugger-elements.service';

@Component({
  selector: 'app-accurate-bounding-view',
  templateUrl: './accurate-bounding-view.component.html',
  styleUrls: ['./accurate-bounding-view.component.scss'],
})
export class AccurateBoundingViewComponent implements OnInit {
  @Input() lastStyleRenderResult!: DebuggerStyleRenderResultModel;

  constructor(readonly debuggerElementsService: DebuggerElementsService) {
  }

  ngOnInit(): void {
  }

  get elementIds(): string[] {
    return Object.keys(this.lastStyleRenderResult.elementIdBoundingRectMap ?? {});
  }
}
