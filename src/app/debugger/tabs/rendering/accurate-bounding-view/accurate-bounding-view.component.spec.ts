import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccurateBoundingViewComponent } from './accurate-bounding-view.component';

describe('AccurateBoundingViewComponent', () => {
  let component: AccurateBoundingViewComponent;
  let fixture: ComponentFixture<AccurateBoundingViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccurateBoundingViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccurateBoundingViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
