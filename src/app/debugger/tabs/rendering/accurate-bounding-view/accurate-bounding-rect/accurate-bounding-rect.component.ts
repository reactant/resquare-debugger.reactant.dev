import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { DebuggerStyleRenderResultModel } from 'src/app/models/debugger-root-container.model';
import { DebuggerElementsService } from 'src/app/services/debugger/debugger-elements.service';
import { MatTooltip } from '@angular/material/tooltip';

@Component({
  selector: 'app-accurate-bounding-rect',
  templateUrl: './accurate-bounding-rect.component.html',
  styleUrls: ['./accurate-bounding-rect.component.scss'],
})
export class AccurateBoundingRectComponent implements OnInit, OnChanges {
  @Input() elementId!: string;
  @Input() isHovering!: boolean;
  @Input() lastStyleRenderResult!: DebuggerStyleRenderResultModel;

  @ViewChild('tooltip') tooltip: MatTooltip | null = null;

  constructor(readonly debuggerElementsService: DebuggerElementsService) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if (this.isHovering) {
      this.tooltip?.show();
    } else {
      this.tooltip?.hide();
    }
  }

  get tooltipMessage(): string {
    const width = this.lastStyleRenderResult.elementIdBoundingRectMap[this.elementId].width;
    const height = this.lastStyleRenderResult.elementIdBoundingRectMap[this.elementId].height;
    return `${width} x ${height}`;
  }

  onMouseEnter(elementId: string): void {
    this.debuggerElementsService.hoveringElementId = elementId;
  }

  onMouseLeave(elementId: string): void {
    if (this.debuggerElementsService.hoveringElementId === elementId) {
      this.debuggerElementsService.hoveringElementId = null;
    }
  }

  expandAllParents(elementId: string): void {
    this.debuggerElementsService.expandAllElementParents(this.debuggerElementsService.elementIdElementMap[elementId]);
  }
}
