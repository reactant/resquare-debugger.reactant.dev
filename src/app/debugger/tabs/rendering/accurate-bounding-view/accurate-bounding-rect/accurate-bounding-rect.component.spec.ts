import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccurateBoundingRectComponent } from './accurate-bounding-rect.component';

describe('AccurateBoundingRectComponent', () => {
  let component: AccurateBoundingRectComponent;
  let fixture: ComponentFixture<AccurateBoundingRectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccurateBoundingRectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccurateBoundingRectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
