import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementTreeViewComponent } from './element-tree-view.component';

describe('ElementTreeViewComponent', () => {
  let component: ElementTreeViewComponent;
  let fixture: ComponentFixture<ElementTreeViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElementTreeViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementTreeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
