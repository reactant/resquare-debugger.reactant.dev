import { Component, Input, OnInit } from '@angular/core';
import { DebuggerElementModel } from 'src/app/models/debugger-root-container.model';
import { DebuggerElementsService } from 'src/app/services/debugger/debugger-elements.service';

@Component({
  selector: 'app-element-tree-view',
  templateUrl: './element-tree-view.component.html',
  styleUrls: ['./element-tree-view.component.scss'],
})
export class ElementTreeViewComponent implements OnInit {
  @Input() elementTree!: DebuggerElementModel;

  constructor(readonly debuggerElementsService: DebuggerElementsService) {
  }


  ngOnInit(): void {
  }

  get selectedElementNode(): null | DebuggerElementModel {
    const { elementIdElementMap } = this.debuggerElementsService;
    return this.debuggerElementsService.selectedElementId == null
      ? null : elementIdElementMap[this.debuggerElementsService.selectedElementId];
  }

  focusElementNode(elementId: string | null): void {
    this.debuggerElementsService.selectedElementId = elementId;
    this.debuggerElementsService.hoveringElementId = elementId;
    if (elementId != null) {
      this.debuggerElementsService.expandAllElementParents(this.debuggerElementsService.elementIdElementMap[elementId]);
    }
  }

  onKeyDownArrayRight(): void {
    if (this.selectedElementNode == null) {
      return;
    }

    if (this.selectedElementNode.children.length > 0) {
      const expanded = this.debuggerElementsService.elementIdExpandStatMap[this.selectedElementNode.id];
      if (expanded) {
        this.focusElementNode(this.selectedElementNode.children[0].id);
      } else {
        this.debuggerElementsService.elementIdExpandStatMap[this.selectedElementNode.id] = true;
      }
    }
  }

  onKeyDownArrayLeft(): void {
    if (this.selectedElementNode == null) {
      return;
    }

    const expanded = this.debuggerElementsService.elementIdExpandStatMap[this.selectedElementNode.id];
    if (expanded) {
      this.debuggerElementsService.elementIdExpandStatMap[this.selectedElementNode.id] = false;
    } else {
      const parentNode = this.debuggerElementsService.elementIdParentMap[this.selectedElementNode.id];
      if (parentNode != null) {
        this.focusElementNode(parentNode.id);
      }
    }
  }

  onKeyDownArrayUp(): void {
    if (this.selectedElementNode == null) {
      return;
    }

    const previousSibling = this.debuggerElementsService.getPreviousSiblingOf(this.selectedElementNode);
    if (previousSibling == null) {
      const parent = this.debuggerElementsService.getParentOf(this.selectedElementNode);
      this.focusElementNode(parent?.id ?? this.selectedElementNode.id);
    } else {
      const searchClosest = (next: DebuggerElementModel): DebuggerElementModel | null => {
        if (this.debuggerElementsService.elementIdExpandStatMap[next.id]) {
          return searchClosest(next.children[next.children.length - 1]);
        } else {
          return next;
        }
      };
      this.focusElementNode(searchClosest(previousSibling)?.id ?? null);
    }
  }

  onKeyDownArrayDown(): void {
    if (this.selectedElementNode == null) {
      return;
    }

    const expanded = this.debuggerElementsService.elementIdExpandStatMap[this.selectedElementNode.id];
    if (expanded) {
      const nextNodeIndex = this.debuggerElementsService.elementIdList.indexOf(this.selectedElementNode.id) + 1;
      if (nextNodeIndex < this.debuggerElementsService.elementIdList.length) {
        this.focusElementNode(this.debuggerElementsService.elementIdList[nextNodeIndex]);
      }
    } else {

      const searchClosest = (next: DebuggerElementModel): DebuggerElementModel | null => {
        const nextParent = this.debuggerElementsService.getParentOf(next);
        const nestSibling = this.debuggerElementsService.getNextSiblingOf(next);
        return nestSibling ?? (nextParent == null ? null : searchClosest(nextParent));
      };
      this.focusElementNode(searchClosest(this.selectedElementNode)?.id ?? this.selectedElementNode.id);
    }
  }

}
