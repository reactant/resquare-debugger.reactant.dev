import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementPropsListComponent } from 'src/app/debugger/tabs/rendering/element-tree-view/element-props-list/element-props-list.component';

describe('ElementPropsListComponent', () => {
  let component: ElementPropsListComponent;
  let fixture: ComponentFixture<ElementPropsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElementPropsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementPropsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
