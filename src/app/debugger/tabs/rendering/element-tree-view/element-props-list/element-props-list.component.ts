import { Component, OnInit } from '@angular/core';
import { DebuggerElementsService } from 'src/app/services/debugger/debugger-elements.service';

@Component({
  selector: 'app-element-props-list',
  templateUrl: './element-props-list.component.html',
  styleUrls: ['./element-props-list.component.scss'],
})
export class ElementPropsListComponent implements OnInit {

  constructor(
    readonly debuggerElementsService: DebuggerElementsService,
  ) {
  }

  ngOnInit(): void {
  }

  get style(): [string, any][] | null {
    return this.debuggerElementsService?.selectedElement?.style == null
      ? null : Object.entries(this.debuggerElementsService.selectedElement.style);
  }

  get item(): any {
    return this.debuggerElementsService?.selectedElement?.item;
  }

  convertStyleType(value: any): any {
    if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
      return value;
    }

    if (value.points != null) {
      return `${value.points}px`;
    } else if (value.percentage != null) {
      return `${value.percentage * 100}%`;
    }

    return JSON.stringify(value);
  }
}
