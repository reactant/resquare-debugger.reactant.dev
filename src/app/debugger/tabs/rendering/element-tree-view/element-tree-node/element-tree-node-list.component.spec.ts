import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementTreeNodeListComponent } from 'src/app/debugger/tabs/rendering/element-tree-view/element-tree-node/element-tree-node-list.component';

describe('ElementTreeNodeComponent', () => {
  let component: ElementTreeNodeListComponent;
  let fixture: ComponentFixture<ElementTreeNodeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElementTreeNodeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementTreeNodeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
