import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { DebuggerElementModel } from 'src/app/models/debugger-root-container.model';
import { DebuggerElementsService } from 'src/app/services/debugger/debugger-elements.service';

const EMPTY: string[] = [];

@Component({
  selector: 'app-element-tree-node-list',
  templateUrl: './element-tree-node-list.component.html',
  styleUrls: ['./element-tree-node-list.component.scss'],
})
export class ElementTreeNodeListComponent implements OnInit{
  @Input() elements!: DebuggerElementModel[];
  @Input() nested!: number;

  constructor(readonly debuggerElementsService: DebuggerElementsService) {
  }

  ngOnInit(): void {
  }

  setSelectedElementIds(value: string[]): void {
    const newValue = value[0];
    if (newValue !== this.debuggerElementsService.selectedElementId) {
      this.debuggerElementsService.selectedElementId = newValue;
    }
  }

  onMouseEnter(elementId: string): void {
    this.debuggerElementsService.hoveringElementId = elementId;
  }

  onMouseLeave(elementId: string): void {
    if (this.debuggerElementsService.hoveringElementId === elementId) {
      this.debuggerElementsService.hoveringElementId = null;
    }
  }
}
