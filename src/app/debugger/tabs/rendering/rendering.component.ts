import { Component, OnInit } from '@angular/core';
import { DebuggerRootContainerModel } from 'src/app/models/debugger-root-container.model';
import { DebuggerElementsService } from 'src/app/services/debugger/debugger-elements.service';
import { DebuggerInterfaceService, DebuggerRootContainerInfo } from 'src/app/services/debugger/debugger-interface.service';

@Component({
  selector: 'app-rendering',
  templateUrl: './rendering.component.html',
  styleUrls: ['./rendering.component.scss'],
})
export class RenderingComponent implements OnInit {
  rootContainer!: DebuggerRootContainerModel | null;

  rootContainersList: DebuggerRootContainerInfo[] | null = [];

  constructor(
    readonly debuggerElementsService: DebuggerElementsService,
    readonly debuggerInterfaceService: DebuggerInterfaceService,
  ) {
  }

  ngOnInit(): void {
    this.debuggerElementsService.getRootContainerObservable().subscribe(it => this.rootContainer = it);
    this.debuggerInterfaceService.getRootContainerListObservable().subscribe(it => this.rootContainersList = it);
  }

  startRenderingInspect(){

  }

  get elementIds(): string[] {
    return Object.keys(this.rootContainer?.lastStyleRenderResult?.elementIdBoundingRectMap ?? {});
  }
}
