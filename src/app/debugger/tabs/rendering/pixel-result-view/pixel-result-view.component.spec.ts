import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PixelResultViewComponent } from './pixel-result-view.component';

describe('PixelResultViewComponent', () => {
  let component: PixelResultViewComponent;
  let fixture: ComponentFixture<PixelResultViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PixelResultViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PixelResultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
