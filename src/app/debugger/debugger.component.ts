import { Component, OnInit } from '@angular/core';
import { DebuggerInterfaceService } from 'src/app/services/debugger/debugger-interface.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-debugger',
  templateUrl: './debugger.component.html',
  styleUrls: ['./debugger.component.scss'],
})
export class DebuggerComponent implements OnInit {

  constructor(
    private debuggerInterfaceService: DebuggerInterfaceService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.route.queryParamMap.pipe(map(paramMap => paramMap.get('targetUrl'))).subscribe((targetUrl) => {
      this.debuggerInterfaceService.connect(targetUrl == null ? 'localhost:27465' : decodeURIComponent(targetUrl));
    });
  }

}
