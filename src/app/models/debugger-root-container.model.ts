export interface DebuggerRootContainerModel {
  viewer: string[];
  width: number;
  height: number;
  title: string;
  multithread: boolean;
  lastStyleRenderResult: DebuggerStyleRenderResultModel | null;
}

export interface DebuggerElementModel {
  type: string;
  id: string;
  style: any | null;
  item: any | null;
  children: DebuggerElementModel[];
}

export interface DebuggerStyleRenderResultModel {
  elementTree: DebuggerElementModel;
  pixelsResult: DebuggerPixel[];
  elementIdBoundingRectMap: Record<string, DebuggerBoundingRectModel>;
}

export interface DebuggerBoundingRectModel {
  x: number;
  y: number;
  width: number;
  height: number;
  zIndex: number;
}

export interface ItemStackModel {
  name: string;
}

export interface DebuggerPixel {
  position: [number, number];
  itemStack: ItemStackModel | null;
  elementId: string | null;
}
