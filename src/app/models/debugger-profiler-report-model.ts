import { DebuggerStyleRenderResultModel } from 'src/app/models/debugger-root-container.model';

export interface DebuggerProfilerRenderTaskModel {
  type: string;
  taskName: string;
  totalTimePeriod: DebuggerTaskTimePeriodModel;
  threadName: string;
}

export interface DebuggerProfilerStyleRenderTaskModel extends DebuggerProfilerRenderTaskModel {
  type: 'StyleRenderTask';
  nodeCreationTimePeriod: DebuggerTaskTimePeriodModel;
  flexboxCalculationTimePeriod: DebuggerTaskTimePeriodModel;
  pixelPaintingTimePeriod: DebuggerTaskTimePeriodModel;
}

export interface DebuggerTaskTimePeriodModel {
  startTime: number | null;
  endTime: number | null;
}

export interface DebuggerProfilerNodeRenderStateModel {
  id: string;
  name: string;
  subNodeRenderStates: DebuggerProfilerNodeRenderStateModel[];
}

export interface DebuggerProfilerNodeStateRenderInfoModel {
  renderReason: string;
}

export interface DebuggerProfilerDOMRenderTaskIterationModel {
  totalTimePeriod: DebuggerTaskTimePeriodModel;
  nodeStateIdTimePeriodMap: Record<string, DebuggerTaskTimePeriodModel>;
  nodeStateIdInfoMap: Record<string, DebuggerProfilerNodeStateRenderInfoModel>;
  profilerNodeRenderState: DebuggerProfilerNodeRenderStateModel;
}

export interface DebuggerRootContainerInfoModel {
  title: string | null;
  viewer: string[];
  width: number;
  height: number;
  multithread: boolean;
}

export interface DebuggerProfilerDOMRenderTaskModel extends DebuggerProfilerRenderTaskModel {
  type: 'DOMRenderTask';
  iterations: DebuggerProfilerDOMRenderTaskIterationModel[];
  totalTimePeriod: DebuggerTaskTimePeriodModel;
  threadName: string;
  rootContainerInfo: DebuggerRootContainerInfoModel;
}

export interface DebuggerProfilerResultModel {
  domRenderTasks: DebuggerProfilerDOMRenderTaskModel[];
  styleRenderTasks: DebuggerProfilerStyleRenderTaskModel[];
  totalTimePeriod: DebuggerTaskTimePeriodModel;
}
