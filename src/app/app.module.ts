import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DebuggerComponent } from './debugger/debugger.component';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { RenderingComponent } from './debugger/tabs/rendering/rendering.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AccurateBoundingViewComponent } from './debugger/tabs/rendering/accurate-bounding-view/accurate-bounding-view.component';
import { ElementTreeViewComponent } from './debugger/tabs/rendering/element-tree-view/element-tree-view.component';
import { ElementTreeNodeListComponent } from 'src/app/debugger/tabs/rendering/element-tree-view/element-tree-node/element-tree-node-list.component';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { ElementPropsListComponent } from './debugger/tabs/rendering/element-tree-view/element-props-list/element-props-list.component';
import { AccurateBoundingRectComponent } from './debugger/tabs/rendering/accurate-bounding-view/accurate-bounding-rect/accurate-bounding-rect.component';
import { MatIconModule } from '@angular/material/icon';
import { PixelResultViewComponent } from './debugger/tabs/rendering/pixel-result-view/pixel-result-view.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ProfilerComponent } from './debugger/tabs/profiler/profiler.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ThreadsTimelineComponent } from './debugger/tabs/profiler/threads-timeline/threads-timeline.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { DomRenderTaskDetailsComponent } from './debugger/tabs/profiler/dom-render-task-details/dom-render-task-details.component';
import { DomRenderTaskIterationComponent } from './debugger/tabs/profiler/dom-render-task-details/dom-render-task-iteration/dom-render-task-iteration.component';
import { DomRenderTaskNodeCostComponent } from './debugger/tabs/profiler/dom-render-task-details/dom-render-task-node-cost/dom-render-task-node-cost.component';
import { StyleRenderTaskDetailsComponent } from './debugger/tabs/profiler/style-render-task-details/style-render-task-details.component';

@NgModule({
  declarations: [
    AppComponent,
    DebuggerComponent,
    RenderingComponent,
    AccurateBoundingViewComponent,
    ElementTreeViewComponent,
    ElementTreeNodeListComponent,
    ElementPropsListComponent,
    AccurateBoundingRectComponent,
    PixelResultViewComponent,
    ProfilerComponent,
    ThreadsTimelineComponent,
    DomRenderTaskDetailsComponent,
    DomRenderTaskIterationComponent,
    DomRenderTaskNodeCostComponent,
    StyleRenderTaskDetailsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTabsModule,
    MatTooltipModule,
    MatListModule,
    MatButtonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    NgScrollbarModule,
    FlexLayoutModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
