import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DebuggerComponent } from 'src/app/debugger/debugger.component';

const routes: Routes = [
  { path: '', component: DebuggerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
