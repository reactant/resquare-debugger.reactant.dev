import { Injectable } from '@angular/core';
import { DebuggerElementModel, DebuggerRootContainerModel } from 'src/app/models/debugger-root-container.model';
import { DebuggerInterfaceService } from 'src/app/services/debugger/debugger-interface.service';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { keyBy as _keyBy } from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class DebuggerElementsService {
  selectedElementId!: string | null;
  hoveringElementId!: string | null;
  elementIdExpandStatMap: Record<string, boolean> = {};
  rootContainerUpdatePaused = false;
  private rootContainerSubject: Subject<DebuggerRootContainerModel> = new ReplaySubject<DebuggerRootContainerModel>(1);


  elementIdElementMap: Record<string, DebuggerElementModel> = {};
  elementIdParentMap: Record<string, DebuggerElementModel> = {};
  elementIdList: string[] = [];

  constructor(private debuggerInterfaceService: DebuggerInterfaceService) {
    debuggerInterfaceService.getReceivedUpdateObservable().pipe(
      filter(() => !this.rootContainerUpdatePaused),
    ).subscribe(it => {
      this.rootContainerSubject.next(it);

      const flattenElements = (elementNode: DebuggerElementModel): DebuggerElementModel[] => [elementNode,
        ...(elementNode.children.flatMap((subNode: DebuggerElementModel) => flattenElements(subNode)))];
      const elementNodes = it.lastStyleRenderResult?.elementTree == null ? [] : flattenElements(it.lastStyleRenderResult?.elementTree);

      this.elementIdElementMap = _keyBy(elementNodes, elementNode => elementNode.id);

      this.elementIdList = Object.keys(this.elementIdElementMap);

      this.elementIdParentMap = {};
      elementNodes.forEach(elementNode => {
        elementNode.children.forEach(childrenNode => {
          this.elementIdParentMap[childrenNode.id] = elementNode;
        });
      });

    });
  }

  get selectedElement(): DebuggerElementModel | null {
    return this.selectedElementId == null ? null : this.elementIdElementMap[this.selectedElementId];
  }

  getParentOf(element: DebuggerElementModel): DebuggerElementModel | null {
    return this.elementIdParentMap[element.id];
  }

  getPreviousSiblingOf(element: DebuggerElementModel): DebuggerElementModel | null {
    const parent = this.getParentOf(element);
    if (parent == null) {
      return null;
    }
    const inChildrenIndex = parent.children.indexOf(element);
    return parent.children[inChildrenIndex - 1];
  }

  getNextSiblingOf(element: DebuggerElementModel): DebuggerElementModel | null {
    const parent = this.getParentOf(element);
    if (parent == null) {
      return null;
    }
    const inChildrenIndex = parent.children.indexOf(element);
    return parent.children[inChildrenIndex + 1];
  }

  expandAllElementParents(element: DebuggerElementModel): void {
    const recursiveExpandParent = (next: DebuggerElementModel): void => {
      const parent = this.getParentOf(next);
      if (parent != null) {
        this.elementIdExpandStatMap[parent.id] = true;
        recursiveExpandParent(parent);
      }
    };
    recursiveExpandParent(element);
  }

  getRootContainerObservable(): Observable<DebuggerRootContainerModel> {
    return this.rootContainerSubject.asObservable();
  }
}
