import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { DebuggerRootContainerModel } from 'src/app/models/debugger-root-container.model';
import { Observable, ReplaySubject, Subject } from 'rxjs';

export interface DebuggerRootContainerInfo {
  id: string;
  title: string;
  viewer: string[];
}

@Injectable({
  providedIn: 'root',
})
export class DebuggerInterfaceService {

  private remoteSubject: WebSocketSubject<{ type: string, data: any }> | null = null;

  private remoteInputSubject: Subject<{ type: string, data: any }> = new Subject<{ type: string; data: any }>();

  remoteInputObservable: Observable<any> = this.remoteInputSubject.asObservable();

  private receivedUpdateSubject: Subject<DebuggerRootContainerModel> = new Subject<DebuggerRootContainerModel>();

  private rootContainerListSubject: Subject<DebuggerRootContainerInfo[]> = new ReplaySubject<DebuggerRootContainerInfo[]>(1);


  private privateRenderingInspectTargetId: string | null = null;

  get renderingInspectTargetId(): string | null {
    return this.privateRenderingInspectTargetId;
  }

  set renderingInspectTargetId(value: string | null) {
    this.privateRenderingInspectTargetId = value;
    this.sendRenderingInspectStart();
  }

  constructor() {
  }

  connect(url: string): void {
    if (this.remoteSubject != null) {
      this.remoteSubject.complete();
    }
    this.remoteSubject = webSocket<{ type: string, data: any }>(url);
    this.remoteSubject.subscribe(this.remoteInputSubject);
    this.remoteSubject.subscribe(it => {
      console.log(it);
      if (it.type === 'RootContainerListUpdate') {
        this.rootContainerListSubject.next(it.data as DebuggerRootContainerInfo[]);
      } else if (it.type === 'RenderingInspectFrame') {
        this.receivedUpdateSubject.next(it.data as DebuggerRootContainerModel);
      }
    });
  }

  getReceivedUpdateObservable(): Observable<DebuggerRootContainerModel> {
    return this.receivedUpdateSubject.asObservable();
  }

  getRootContainerListObservable(): Observable<DebuggerRootContainerInfo[]> {
    return this.rootContainerListSubject.asObservable();
  }

  send(type: string, data: any): void {
    this.remoteSubject?.next({ type, data });
  }

  sendRenderingInspectStart(): void {
    this.remoteSubject?.next({
      type: 'RenderingInspectStart',
      data: this.renderingInspectTargetId,
    });
  }

  disconnect(): void {
    this.remoteSubject?.complete();
    this.remoteSubject = null;
  }
}
