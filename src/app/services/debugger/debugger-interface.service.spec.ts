import { TestBed } from '@angular/core/testing';

import { DebuggerInterfaceService } from 'src/app/services/debugger/debugger-interface.service';

describe('DebuggerInterfaceService', () => {
  let service: DebuggerInterfaceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DebuggerInterfaceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
