import { Injectable } from '@angular/core';
import { DebuggerInterfaceService } from 'src/app/services/debugger/debugger-interface.service';
import { filter } from 'rxjs/operators';
import { DebuggerProfilerRenderTaskModel, DebuggerProfilerResultModel } from 'src/app/models/debugger-profiler-report-model';
import { groupBy as _groupBy } from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class DebuggerProfilerService {

  isProfiling = false;
  isWaitingForProfilingResult = false;

  profilerResult: DebuggerProfilerResultModel | null = null;
  profilerResultThreadsTasks: { threadName: string, tasks: DebuggerProfilerRenderTaskModel[] }[] | null = null;
  profilerResultTotalNanosecond = 0;


  constructor(private debuggerInterfaceService: DebuggerInterfaceService) {
    debuggerInterfaceService.remoteInputObservable?.pipe(
      filter(it => it.type === 'ProfilingResult'),
    ).subscribe(({ data: profilerResult }: { data: DebuggerProfilerResultModel }) => {
      this.isWaitingForProfilingResult = false;

      this.profilerResult = profilerResult;
      this.profilerResultThreadsTasks = Object.entries(
        _groupBy([...profilerResult.domRenderTasks, ...profilerResult.styleRenderTasks], task => task.threadName),
      ).map(([threadName, tasks]) => ({
          threadName,
          tasks,
        }),
      );
      // tslint:disable-next-line:no-non-null-assertion
      this.profilerResultTotalNanosecond = profilerResult.totalTimePeriod.endTime! - profilerResult.totalTimePeriod.startTime!;
    });
  }

  startProfiling(): void {
    if (this.isProfiling) {
      throw new Error('Already profiling');
    }
    this.isProfiling = true;
    this.debuggerInterfaceService.send('ProfilingStart', null);
  }

  stopProfiling(): void {
    if (!this.isProfiling) {
      throw new Error('Not profiling');
    }
    this.isWaitingForProfilingResult = true;
    this.isProfiling = false;
    this.debuggerInterfaceService.send('ProfilingStop', null);
  }
}
