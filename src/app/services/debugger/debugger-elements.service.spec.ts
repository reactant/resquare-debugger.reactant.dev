import { TestBed } from '@angular/core/testing';

import { DebuggerElementsService } from './debugger-elements.service';

describe('DebuggerElementsService', () => {
  let service: DebuggerElementsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DebuggerElementsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
