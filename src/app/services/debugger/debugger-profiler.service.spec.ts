import { TestBed } from '@angular/core/testing';

import { DebuggerProfilerService } from './debugger-profiler.service';

describe('DebuggerProfilerService', () => {
  let service: DebuggerProfilerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DebuggerProfilerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
